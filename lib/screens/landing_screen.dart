import "package:flutter/material.dart";
import 'register_screen.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'login_screen.dart';

class LandingScreen extends StatelessWidget {
  const LandingScreen({Key? key}) : super(key: key);
  static const routeName = '/landing';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding: const EdgeInsets.all(10),
      child: SizedBox(
        width: double.infinity,
        child: Column(
          children: [
            const Image(image: AssetImage("assets/seneca.png")),
            Column(
              children: [
                ElevatedButton(
                  onPressed: () => {
                    Navigator.pushNamed(context, LoginScreen.routeName),
                  },
                  child: const Text("Login"),
                  style: ElevatedButton.styleFrom(
                    shape: const StadiumBorder(),
                    minimumSize: const Size(150, 40),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                    onPressed: () => {
                          Navigator.pushNamed(
                              context, RegisterScreen.routeName),
                        },
                    child: const Text("Sign up"),
                    style: ElevatedButton.styleFrom(
                        minimumSize: const Size(150, 40),
                        onPrimary: Colors.black,
                        shape: const StadiumBorder(),
                        primary: const Color.fromRGBO(113, 247, 236, 1))),
              ],
            ),
            const Text("Version 0.1.0")
          ],
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
        ),
      ),
    ));
  }
}
