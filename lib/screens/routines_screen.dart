import 'package:cloud_firestore/cloud_firestore.dart';
import "package:flutter/material.dart";
import 'package:gymtastic/models/routines_data.dart';
import 'package:gymtastic/screens/add_exercises_to_routine.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import '../models/exercises_data.dart';
import '../services/connection_service.dart';
import 'ExerciseDetail.dart'; 

class RoutineScreen extends StatefulWidget {
  const RoutineScreen({Key? key}) : super(key: key);
  static const routeName = '/routine';
  @override
  _RoutineScreenState createState() => _RoutineScreenState();
}

class _RoutineScreenState extends State<RoutineScreen> {

  var _isOnline = true; 
  final ConnectionService _connectionService = ConnectionService(); 

 final _routines_box = Hive.box<RoutinesData>('routines_local');
 
  @override
  void initState() {
    super.initState(); 
    _connectionService.initialize(); 
    _connectionService.myStream.listen((event) { 
      if (mounted) { 
        setState(() { 
          _isOnline = event; 
        }); 
      } 
    }); 
  }


  Widget buildExercises(BuildContext context, String name, String type) {
  return Container(
    alignment: Alignment.centerLeft,
    child: RaisedButton(
        color: Color.fromRGBO(255, 254, 229, 1),
        textColor: Color.fromRGBO(20, 51, 51, 1),
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder:(context)=> const ExerciseDetail())
          );
          },
        child: Row(
          mainAxisAlignment:
              MainAxisAlignment.spaceBetween,
          children: [
            Text(name),
            Text(type),
            Icon(Icons.arrow_circle_up_sharp),
          ],
        )
      )
    );
  }

  Widget buildRoutines(BuildContext context, RoutinesData routine) {
  return Column(
    children: [
      Container(
        decoration: BoxDecoration(
        border: Border.all(
          color: const Color.fromRGBO(196, 196, 196, 0.5),
        ),
        borderRadius: const BorderRadius.all(Radius.circular(20))
      ),
        child: Text(
              routine.name,
              style: const TextStyle(
                  fontSize: 24,
                  color: Color.fromRGBO(63, 73, 71, 1),
                  letterSpacing: 0.1),
            ),
      ),
      Container(
        child: ListView.builder(
          shrinkWrap: true,
          physics: ScrollPhysics(),
          padding: EdgeInsets.all(8),
          itemCount: routine.exercises.length,
          itemBuilder: (BuildContext context, int index) {

            final exercise = routine.exercises[index];
            return buildExercises(context, exercise.name, exercise.type);
          },
        ),
      )
    ]
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: SizedBox(
        width: double.infinity,
        child: Column(
          children: [
           const Text(
              'Routines',
              style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(63, 73, 71, 1),
                  letterSpacing: 0.1),
            ),
            ElevatedButton.icon(
              onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder:(context)=> const AddExcersiseRoutineScreen()));
              },
              icon: Icon(Icons.add, size: 18),
              label: Text("Add a new routine"),
            ),
            Container(
              height: 300,
              width: double.infinity,
              child: SingleChildScrollView(
                physics: ScrollPhysics(),
                child: ValueListenableBuilder<Box<RoutinesData>>(
                    valueListenable: _routines_box.listenable(),
                    builder: (context, box, _) {
                      Widget resp;
                      final routines = box.values.toList().cast<RoutinesData>();
                      if (routines.isEmpty) {
                        resp =  const Center(
                          child: Text(
                            'No routines stored locally! Add a new one!',
                            style: TextStyle(fontSize: 24),
                          ),
                        );
                      }
                      else{
                         resp = Container(
                          child: ListView.builder(
                            shrinkWrap: true,
                            physics: ScrollPhysics(),
                            padding: EdgeInsets.all(8),
                            itemCount: routines.length,
                            itemBuilder: (BuildContext context, int index) {

                              final exercise = routines[index];
                              return buildRoutines(context, routines.elementAt(index));
                            },
                          ),
                        );
                      }
                      return resp;
                    }
                  )
                )
              )
          ],
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
        )
      )
    );
  }
}