import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';


class ExerciseDetail extends StatefulWidget {
  const ExerciseDetail({Key? key}) : super(key: key);
  @override
  _ExerciseDetail createState() => _ExerciseDetail();
}

class _ExerciseDetail extends State<ExerciseDetail> {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
  @override 
  Widget build(BuildContext contet){
    return Scaffold(
      body: Padding(
      padding: const EdgeInsets.all(10),
     
        child: Column(
    
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Container(
              
              width: MediaQuery.of(context).size.width-40,
              height: MediaQuery.of(context).size.height/3,


              child: Image.network("https://i.blogs.es/a13680/1366_2000/840_560.jpeg"  ),
              ),
              TextField(

                    textAlign: TextAlign.center,
                    decoration: const InputDecoration(labelText: "Weight"),
                    inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    keyboardType: const TextInputType.numberWithOptions(decimal: true),

              ),
              TextField(

                    textAlign: TextAlign.center,
                    decoration: const InputDecoration(labelText: "Repetitions"),
                    inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    keyboardType: const TextInputType.numberWithOptions(decimal: true),
              ),  
                
              ElevatedButton(onPressed: (){
                Navigator.pop(context);
              }, 
              child: const Text("Go to the next exercise!"),
              style: ElevatedButton.styleFrom(
                        minimumSize: const Size(300, 40),
                        onPrimary: Colors.black,
                        shape: const StadiumBorder(),
                        primary: const Color.fromRGBO(113, 247, 236, 1)),
                        
              )
          ],
        ),
      ),
    )
    ;
}
}
