import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:gymtastic/screens/health_form_screen.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../services/connection_service.dart';
import '../models/register_data.dart';
import '../models/health_form_arguments.dart';
import '../services/local_storage_service.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);
  static const routeName = '/register';

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  FirebaseAuth auth = FirebaseAuth.instance;
  CollectionReference users = FirebaseFirestore.instance.collection('users');
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _confirmPasswordController = TextEditingController();
  final _localStorageService = LocalStorageService();
  var _isOnline = true;
  var _isLoading = false;
  var _showPassword = false;
  var _showConfirmPassword = false;
  final _formKey = GlobalKey<FormState>();

  final ConnectionService _connectionService = ConnectionService();

  Future<void> _submitData(BuildContext ctx) async {
    FocusManager.instance.primaryFocus?.unfocus();
    setState(() {
      _isLoading = true;
    });
    try {
      UserCredential userCredential = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(
              email: _emailController.text, password: _passwordController.text);
      await addUser();
      if (userCredential.user != null) {
        _localStorageService.removeItem('registerData');
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text("User created successfully"),
            backgroundColor: Colors.green,
          ),
        );
        Navigator.pushReplacementNamed(context, HealthFormScreen.routeName,
            arguments: HealthFormArguments(justOne: false, selectedWidget: 0));
      }
    } on FirebaseAuthException catch (e, stacktrace) {
      await FirebaseCrashlytics.instance
          .recordError(e, stacktrace, reason: 'Error in RegisterScreen');
      if (e.code == 'email-already-in-use') {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text("Email already in use"),
            backgroundColor: Colors.red,
          ),
        );
      }
    } catch (e, stacktrace) {
      await FirebaseCrashlytics.instance
          .recordError(e, stacktrace, reason: 'Error in RegisterScreen');
      offlineRegister(ctx);
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text("An error occured, try again"),
          backgroundColor: Colors.red,
        ),
      );
    }
    setState(() {
      _isLoading = false;
    });
  }

  Future<void> addUser() async {
    await users.doc(auth.currentUser!.uid).set({
      'email': _emailController.text,
      'name': _nameController.text,
    });

    await FirebaseFirestore.instance
        .collection('weight_history')
        .doc(auth.currentUser!.uid)
        .set({
      'email': _emailController.text,
    });
  }

  String? _checkName(String? name) {
    if (name!.isEmpty || name.length < 3 || name.length > 20) {
      return "Name should be between 3 and 20 characters";
    }
    return null;
  }

  String? _checkPassword(String? password) {
    if (password!.isEmpty || password.length < 6 || password.length > 20) {
      return "Password should be between 6 and 20 characters";
    }
    return null;
  }

  String? _checkConfirmPassword(String? confirmPassword) {
    if (confirmPassword != _passwordController.text) {
      return "Passwords do not match";
    }
    return null;
  }

  String? _checkEmail(String? email) {
    if (email!.isEmpty ||
        !RegExp(r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
            .hasMatch(email)) {
      return "Please enter a valid email";
    } else {}
    return null;
  }

  @override
  void dispose() {
    _nameController.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _confirmPasswordController.dispose();

    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _localStorageService.getRegisterData().then((data) => {
          if (data != null)
            {
              _nameController.text = data.name,
              _emailController.text = data.email,
              _localStorageService.removeItem('registerData'),
            }
        });
    _connectionService.initialize();
    _connectionService.myStream.listen((event) {
      if (mounted) {
        setState(() {
          _isOnline = event;
        });
      }
    });
  }

  void offlineRegister(BuildContext ctx) {
    final registerData = RegisterData(
      email: _emailController.text,
      name: _nameController.text,
    );

    _localStorageService.setItem('registerData', registerData);

    if (!_isOnline) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          backgroundColor: Colors.red,
          content: const Text("No internet connection"),
          action: SnackBarAction(
            textColor: Colors.white,
            label: 'Dismiss',
            onPressed: () {
              ScaffoldMessenger.of(context).hideCurrentSnackBar();
            },
          ),
          duration: const Duration(days: 365),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
          child: SizedBox(
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Text(
                  "CREATE ACCOUNT",
                  style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(63, 73, 71, 1),
                      letterSpacing: 0.1),
                ),
                SizedBox(
                  width: 250,
                  child: TextFormField(
                    controller: _nameController,
                    validator: _checkName,
                    decoration: InputDecoration(
                        errorMaxLines: 2,
                        border: const OutlineInputBorder(),
                        labelText: "Name",
                        suffixIcon: IconButton(
                          icon: const Icon(
                            Icons.cancel,
                          ),
                          onPressed: () {
                            setState(() {
                              _nameController.clear();
                            });
                          },
                        ),
                        floatingLabelBehavior: FloatingLabelBehavior.always),
                  ),
                ),
                SizedBox(
                  width: 250,
                  child: TextFormField(
                    controller: _emailController,
                    validator: _checkEmail,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                        border: const OutlineInputBorder(),
                        labelText: "Email",
                        suffixIcon: IconButton(
                          icon: const Icon(Icons.cancel),
                          onPressed: () => {
                            setState(() {
                              _emailController.clear();
                            })
                          },
                        ),
                        floatingLabelBehavior: FloatingLabelBehavior.always),
                  ),
                ),
                SizedBox(
                  width: 250,
                  child: TextFormField(
                    controller: _passwordController,
                    validator: _checkPassword,
                    obscureText: !_showPassword,
                    decoration: InputDecoration(
                        border: const OutlineInputBorder(),
                        labelText: "Password",
                        errorMaxLines: 2,
                        suffixIcon: IconButton(
                          icon: Icon(
                            (_showPassword)
                                ? Icons.visibility
                                : Icons.visibility_off,
                            color: Colors.teal,
                          ),
                          onPressed: () {
                            setState(() {
                              _showPassword = !_showPassword;
                            });
                          },
                        ),
                        floatingLabelBehavior: FloatingLabelBehavior.always),
                  ),
                ),
                SizedBox(
                  width: 250,
                  child: TextFormField(
                    controller: _confirmPasswordController,
                    validator: _checkConfirmPassword,
                    obscureText: !_showConfirmPassword,
                    decoration: InputDecoration(
                        border: const OutlineInputBorder(),
                        labelText: "Confirm password",
                        errorMaxLines: 2,
                        suffixIcon: IconButton(
                          icon: Icon(
                            (_showConfirmPassword)
                                ? Icons.visibility
                                : Icons.visibility_off,
                            color: Colors.teal,
                          ),
                          onPressed: () {
                            setState(() {
                              _showConfirmPassword = !_showConfirmPassword;
                            });
                          },
                        ),
                        floatingLabelBehavior: FloatingLabelBehavior.always),
                  ),
                ),
                ElevatedButton(
                  onPressed: _isOnline
                      ? () => {
                            if (_formKey.currentState!.validate())
                              {_submitData(context)}
                          }
                      : () => {offlineRegister(context)},
                  child: (_isLoading)
                      ? const CircularProgressIndicator(
                          backgroundColor: Colors.white,
                        )
                      : const Text("CONTINUE"),
                  style: ElevatedButton.styleFrom(
                    shape: const StadiumBorder(),
                    minimumSize: const Size(250, 40),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
