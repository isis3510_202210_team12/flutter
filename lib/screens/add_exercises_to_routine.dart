import 'package:cloud_firestore/cloud_firestore.dart';
import "package:flutter/material.dart";
import 'package:gymtastic/screens/routines_screen.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import '../models/ExerciseDTO.dart';
import '../models/exercises_data.dart';
import '../models/routines_data.dart';
import '../services/connection_service.dart'; 

class AddExcersiseRoutineScreen extends StatefulWidget {
  const AddExcersiseRoutineScreen({Key? key}) : super(key: key);
  static const routeName = '/addExeToRoutine';
  @override
  _AddExcersiseRoutineScreenState createState() => _AddExcersiseRoutineScreenState();
}

class _AddExcersiseRoutineScreenState extends State<AddExcersiseRoutineScreen> {

  bool flag = false;

  Map<int, bool> _selectedExercises = {};

  final _nameController = TextEditingController();

  var _isOnline = true; 
  final ConnectionService _connectionService = ConnectionService(); 

  final _routines_box = Hive.box<RoutinesData>('routines_local');
  final _exercises_box = Hive.box<ExercisesData>('exercises_local');

  final _formKey = GlobalKey<FormState>();
 
  @override
  void initState() {
    super.initState(); 
    _connectionService.initialize(); 
    _connectionService.myStream.listen((event) { 
      if (mounted) { 
        setState(() { 
          _isOnline = event; 
        }); 
      } 
    }); 
  }

Future<QuerySnapshot> _callExercises() async{
     Future<QuerySnapshot> _exercises =
      FirebaseFirestore.instance.collection("exercises").get();
    return _exercises;
  }

  void _submitRoutine(String name) async{
    List<ExercisesData> ejercicios = [];
    _selectedExercises.forEach((key, value) {
      if (value) {
        ejercicios.add(_exercises_box.get(key)!);
      }
    });
    final rutina = RoutinesData()
    ..name = name
    ..exercises = ejercicios;
    await _routines_box.add(rutina);
  }

  Future addExercises(int key , String name, String type) async{
    final exercise = ExercisesData()
    ..name = name
    ..type = type;
    await _exercises_box.put(key, exercise);
  }

  Widget buildExercises(BuildContext context, String name, String type, int index) {
    var selected = false;
    if(_selectedExercises[index] != null){
      selected = _selectedExercises[index]!;
    }
    return Container(
      alignment: Alignment.centerLeft,
      child: RaisedButton(
          color: selected? Color.fromARGB(255, 133, 30, 52) :Color.fromRGBO(255, 254, 229, 1),
          textColor: selected? Color.fromARGB(255, 255, 255, 255): Color.fromRGBO(20, 51, 51, 1),
          onPressed: () {
              if(_selectedExercises[index] == null){
                _selectedExercises[index] = true;
              }
              else{
                _selectedExercises[index] = !_selectedExercises[index]!;
              }
              print(_selectedExercises);
            },
          child: Row(
            mainAxisAlignment:
                MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(width: 10),
              Text(name, style: TextStyle(fontSize: 16),),
              SizedBox(width: 10),
              Text(type,  style: TextStyle(fontSize: 16)),
              SizedBox(width: 10),
            ],
          )
        )
      );
  }

   Widget onlineData(BuildContext context){
    return FutureBuilder<QuerySnapshot>(
      future: _callExercises(),
      builder: (context,
        AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return ListView.builder(
            shrinkWrap: true,
            physics: ScrollPhysics(),
            itemCount: snapshot.data!.docs.length,
            itemBuilder: (_, index) {
              QueryDocumentSnapshot x = snapshot.data!.docs[index];
              ExerciseDTO ejercicio = ExerciseDTO(x["name"]);
              addExercises(index, ejercicio.name, ejercicio.type);
              return buildExercises( context, ejercicio.name, ejercicio.type, index);
            },
          );
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
      width: double.infinity,
      child: Form(
        key: _formKey,
        child: Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
            const Text(
              "Create your new routine!",
              style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(63, 73, 71, 1),
                  letterSpacing: 0.1),
            ),
            SizedBox(
              width: 200,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  TextFormField(
                    controller: _nameController,
                    decoration: InputDecoration(
                        border: const OutlineInputBorder(),
                        labelText: "Name",
                        suffixIcon: IconButton(
                          icon: const Icon(
                            Icons.cancel,
                          ),
                          onPressed: () {
                            setState(() {
                              _nameController.clear();
                            });
                          },
                        ),
                        floatingLabelBehavior: FloatingLabelBehavior.always),
                  )
                ],
              ),
            ),
            Container(
              height: 200,
              width: double.infinity,
              child: SingleChildScrollView(
                physics: ScrollPhysics(),
                child: _isOnline? onlineData(context) 
                : ValueListenableBuilder<Box<ExercisesData>>(
                    valueListenable: _exercises_box.listenable(),
                    builder: (context, box, _) {
                      Widget resp;
                      final exercises = box.values.toList().cast<ExercisesData>();
                      if (exercises.isEmpty) {
                        resp =  const Center(
                          child: Text(
                            'No exercises stored locally!',
                            style: TextStyle(fontSize: 24),
                          ),
                        );
                      }
                      else{
                          print("entra a local");
                          resp = Container(
                          child: ListView.builder(
                            shrinkWrap: true,
                            physics: ScrollPhysics(),
                            padding: EdgeInsets.all(8),
                            itemCount: exercises.length,
                            itemBuilder: (BuildContext context, int index) {

                              final exercise = exercises[index];
                              return buildExercises(context, exercise.name, exercise.type, index);
                            },
                          ),
                        );
                      }
                      return resp;
                    }
                  )
                )
              ),
            ElevatedButton(
              onPressed: () => {
                  _selectedExercises.forEach((key, value) {
                  flag = flag || value;
                }),
                if(_selectedExercises.isEmpty){
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      backgroundColor: Colors.red,
                      content: Text("Select at least one exercise!"),
                      action: SnackBarAction(
                        textColor: Colors.white,
                        label: 'Dismiss',
                        onPressed: () {
                          ScaffoldMessenger.of(context).hideCurrentSnackBar();
                        },
                      ),
                      duration: const Duration(days: 365),
                    ),
                )
              }
              else if(flag == false){
                ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      backgroundColor: Colors.red,
                      content: Text("Select at least one exercise!"),
                      action: SnackBarAction(
                        textColor: Colors.white,
                        label: 'Dismiss',
                        onPressed: () {
                          ScaffoldMessenger.of(context).hideCurrentSnackBar();
                        },
                      ),
                      duration: const Duration(days: 365),
                    ),
                )
              }
              else if(_nameController.text == null || _nameController.text.isEmpty){
                ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      backgroundColor: Colors.red,
                      content: Text("Submit a name for your routine!"),
                      action: SnackBarAction(
                        textColor: Colors.white,
                        label: 'Dismiss',
                        onPressed: () {
                          ScaffoldMessenger.of(context).hideCurrentSnackBar();
                        },
                      ),
                      duration: const Duration(days: 365),
                    ),
                )
              }
              else{
                _submitRoutine(_nameController.text),
                Navigator.maybePop(context)
                }
              },
              child: 
              const Text("Add routine!",
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w800,
                )),
              style: ElevatedButton.styleFrom(
                shape: const StadiumBorder(),
                minimumSize: const Size(200, 42),
              ),
            ),
          ]),
        ),
      ),
    ));
  }
  
}