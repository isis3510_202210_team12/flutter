import 'package:cloud_firestore/cloud_firestore.dart';
import "package:flutter/material.dart";
import 'package:gymtastic/screens/ExerciseDetail.dart';
import 'package:gymtastic/models/ExerciseDTO.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:intl/intl.dart';
import '../models/exercises_data.dart';
import '../services/connection_service.dart'; 

class FeedScreen extends StatefulWidget {
  const FeedScreen({Key? key}) : super(key: key);
  static const routeName = '/feed';
  @override
  _FeedScreenState createState() => _FeedScreenState();
}

class _FeedScreenState extends State<FeedScreen> {

  var _isOnline = true; 
  final ConnectionService _connectionService = ConnectionService(); 

 final _exercises_box = Hive.box<ExercisesData>('exercises_local');

  Future<QuerySnapshot> _callExercises() async{
     Future<QuerySnapshot> _exercises =
      FirebaseFirestore.instance.collection("exercises").get();
    return _exercises;
  }

  Future addExercises(int key , String name, String type) async{
    final exercise = ExercisesData()
    ..name = name
    ..type = type;
    await _exercises_box.put(key, exercise);
  }
  
  @override
  void initState() {
    super.initState(); 
    _connectionService.initialize(); 
    _connectionService.myStream.listen((event) { 
      if (mounted) { 
        setState(() { 
          _isOnline = event; 
        }); 
      } 
    }); 
  }

  Widget onlineData(BuildContext context){
    return FutureBuilder<QuerySnapshot>(
      future: _callExercises(),
      builder: (context,
        AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return ListView.builder(
            shrinkWrap: true,
            physics: ScrollPhysics(),
            itemCount: snapshot.data!.docs.length,
            itemBuilder: (_, index) {
              QueryDocumentSnapshot x = snapshot.data!.docs[index];
              ExerciseDTO ejercicio = ExerciseDTO(x["name"]);
              addExercises(index, ejercicio.name, ejercicio.type);
              return buildExercises( context, ejercicio.name, ejercicio.type);
            },
          );
        }
        return Center(child: CircularProgressIndicator());
        
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: SizedBox(
        width: double.infinity,
        child: Column(
          children: [
            Text(
              'Today is '+ DateFormat('EEEE').format(DateTime.now()).toString() +', are you ready?',
              style: const TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(63, 73, 71, 1),
                  letterSpacing: 0.1),
            ),
            Container(
              height: 300,
              width: double.infinity,
              child: SingleChildScrollView(
                physics: ScrollPhysics(),
                child: _isOnline? onlineData(context) 
                : ValueListenableBuilder<Box<ExercisesData>>(
                    valueListenable: _exercises_box.listenable(),
                    builder: (context, box, _) {
                      final exercises = box.values.toList().cast<ExercisesData>();
                      Widget resp;
                      if (exercises.isEmpty) {
                            'No exercises stored locally!',
                        resp =  const Center(
                          child: Text(
                            style: TextStyle(fontSize: 24),
                          ),
                        );
                      }
                      else{
                          print("entra a local");
                         resp = Container(
                          child: ListView.builder(
                            shrinkWrap: true,
                            physics: ScrollPhysics(),
                            padding: EdgeInsets.all(8),
                            itemCount: exercises.length,
                            itemBuilder: (BuildContext context, int index) {

                              final exercise = exercises[index];
                              return buildExercises(context, exercise.name, exercise.type);
                            },
                      }
                        );
                          ),
                      return resp;
                    }
                  )
                )
              )
          ],
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
        )
      )
    );
  }

Widget buildExercises(BuildContext context, String name, String type) {
  return Container(
    alignment: Alignment.centerLeft,
    child: RaisedButton(
        color: Color.fromRGBO(255, 254, 229, 1),
        textColor: Color.fromRGBO(20, 51, 51, 1),
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder:(context)=> const ExerciseDetail())
          );
          },
        child: Row(
          mainAxisAlignment:
              MainAxisAlignment.spaceBetween,
          children: [
            Text(name),
            Text(type),
            Icon(Icons.arrow_circle_up_sharp),
          ],
        )
      )
    );
  }
}