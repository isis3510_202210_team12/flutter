import 'package:flutter/material.dart';
import '../widgets/check_bpm.dart';
import '../models/health_form_arguments.dart';
import '../widgets/about_questions.dart';
import '../widgets/take_photo.dart';

class HealthFormScreen extends StatefulWidget {
  const HealthFormScreen({Key? key}) : super(key: key);
  static const routeName = '/health_form';
  static const widgetsIndex = {
    'formAboutYou': 0,
    'checkBPM': 1,
    'takePhoto': 2,
  };

  @override
  State<HealthFormScreen> createState() => _HealthFormScreenState();
}

class _HealthFormScreenState extends State<HealthFormScreen> {
  void goToNext() {
    if (_selectedIndex + 1 == 3) {
      print('Me salí');
    } else {
      setState(() {
        print('Aumento e 1');
        _selectedIndex++;
      });
    }
  }

  final titles = [
    const Text('Personal information'),
    const Text('Check your BPM'),
    const Text('Take a photo'),
  ];

  var _prevIndex = 0;
  var _selectedIndex = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final args =
        ModalRoute.of(context)!.settings.arguments as HealthFormArguments;
    print('args: ${args.selectedWidget}');
    setState(() {
      if (_prevIndex != _selectedIndex) {
        _prevIndex = _selectedIndex;
      } else {
        _selectedIndex = args.selectedWidget;
      }
    });

    final widgets = [
      AboutQuestions(onNext: goToNext),
      CheckBPM(onNext: goToNext),
      TakePhoto(),
    ];

    return Scaffold(
        appBar: AppBar(
          title: titles[_selectedIndex],
          automaticallyImplyLeading: false,
        ),
        body: Center(child: widgets[_selectedIndex]));
  }
}
