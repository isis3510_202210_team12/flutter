import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'home_screen.dart';
import '../services/connection_service.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);
  static const routeName = '/login';
  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  var _isLoading = false;
  var _showPassword = false;

  final ConnectionService _connectionService = ConnectionService();

  bool _isOnline = false;

  @override
  void initState() {
    super.initState();
    _connectionService.initialize();
    _connectionService.myStream.listen((event) {
      if (mounted) {
        setState(() {
          _isOnline = event;
          if (!_isOnline) {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                backgroundColor: Colors.red,
                content: Text("No internet connection"),
                action: SnackBarAction(
                  textColor: Colors.white,
                  label: 'Dismiss',
                  onPressed: () {
                    ScaffoldMessenger.of(context).hideCurrentSnackBar();
                  },
                ),
                duration: const Duration(days: 365),
              ),
            );
          }
        });
      }
    });
  }

  FirebaseAuth auth = FirebaseAuth.instance;
  final _formKey = GlobalKey<FormState>();
  String? _checkEmail(String? email) {
    if (email == null ||
        email.isEmpty ||
        !RegExp(r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
            .hasMatch(email)) {
      return 'Please enter a valid email';
    }

    return null;
  }

  String? _checkPassword(String? password) {
    if (password == null || password.isEmpty) {
      return 'Please enter a valid password';
    }
    return null;
  }

  Future<void> _submitData(BuildContext ctx) async {
    FocusManager.instance.primaryFocus?.unfocus();
    setState(() {
      _isLoading = true;
    });
    try {
      UserCredential userCredential = await FirebaseAuth.instance
          .signInWithEmailAndPassword(
              email: _emailController.text, password: _passwordController.text);

      if (userCredential.user != null) {
        Navigator.of(context).pushNamedAndRemoveUntil(
            HomeScreen.routeName, (Route<dynamic> route) => false);
      }
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text("User not found"),
            backgroundColor: Colors.red,
          ),
        );
      } else if (e.code == 'wrong-password') {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text("User not found"),
            backgroundColor: Colors.red,
          ),
        );
      }
    }
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SizedBox(
      width: double.infinity,
      child: Form(
        key: _formKey,
        child:
            Column(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
          const Text(
            "CREATE ACCOUNT",
            style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(63, 73, 71, 1),
                letterSpacing: 0.1),
          ),
          SizedBox(
            width: 250,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                TextFormField(
                  controller: _emailController,
                  validator: _checkEmail,
                  decoration: InputDecoration(
                      border: const OutlineInputBorder(),
                      labelText: "Email",
                      suffixIcon: IconButton(
                        icon: const Icon(
                          Icons.cancel,
                        ),
                        onPressed: () {
                          setState(() {
                            _emailController.clear();
                          });
                        },
                      ),
                      floatingLabelBehavior: FloatingLabelBehavior.always),
                ),
                const SizedBox(
                  height: 40,
                ),
                TextFormField(
                  controller: _passwordController,
                  obscureText: !_showPassword,
                  validator: _checkPassword,
                  decoration: InputDecoration(
                      border: const OutlineInputBorder(),
                      labelText: "Password",
                      suffixIcon: IconButton(
                        icon: Icon(
                          (_showPassword)
                              ? Icons.visibility
                              : Icons.visibility_off,
                        ),
                        onPressed: () {
                          setState(() {
                            _showPassword = !_showPassword;
                          });
                        },
                      ),
                      floatingLabelBehavior: FloatingLabelBehavior.always),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    TextButton(
                        onPressed: () {},
                        child: const Text("Forgot Password?")),
                  ],
                ),
              ],
            ),
          ),
          ElevatedButton(
            onPressed: _isOnline
                ? () => {
                      if (_formKey.currentState!.validate())
                        {_submitData(context)}
                    }
                : null,
            child: (_isLoading)
                ? const CircularProgressIndicator(
                    backgroundColor: Colors.white,
                  )
                : const Text("Lets GO!",
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w800,
                    )),
            style: ElevatedButton.styleFrom(
              shape: const StadiumBorder(),
              minimumSize: const Size(250, 42),
            ),
          ),
        ]),
      ),
    ));
  }
}
