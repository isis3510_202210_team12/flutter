import 'package:flutter/material.dart';
import 'package:gymtastic/screens/feed_screen.dart';
import 'package:gymtastic/screens/routines_screen.dart';
import '../widgets/user_profile.dart';
import '../widgets/qr_scan.dart';
import '../services/connection_service.dart';
import '../widgets/chart_weight.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);
  static const routeName = '/home';
  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final ConnectionService _connectionService = ConnectionService();
  var _isOnline = true;
  var _selectedIndex = 0;

  final _pages = [
    const FeedScreen(),
    const RoutineScreen(),
    const Center(
      child: ChartWeight(),
    ),
    UserProfile(),
  ];

  @override
  void initState() {
    super.initState();
    _connectionService.initialize();
    _connectionService.myStream.listen((event) {
      if (mounted) {
        setState(() {
          _isOnline = event;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _pages[_selectedIndex],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color.fromRGBO(113, 247, 236, 1),
        child: const Icon(Icons.qr_code_2_rounded),
        onPressed: () => {
          _connectionService.myStream.listen((event) {
            if (mounted) {
              setState(() {
                _isOnline = event;
              });
            }
          }),
          if (!_isOnline)
            {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  backgroundColor: Colors.red,
                  content: const Text("No internet connection"),
                  action: SnackBarAction(
                    textColor: Colors.white,
                    label: 'Dismiss',
                    onPressed: () {
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    },
                  ),
                  duration: const Duration(days: 365),
                ),
              )
            }
          else
            {Navigator.pushNamed(context, QRScanPage.routeName)}
        },
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedIndex,
        onTap: (index) {
          print("Este es el indice");
          setState(() {
            _selectedIndex = index;
          });
        },
        unselectedItemColor: Colors.grey,
        selectedItemColor: Colors.black,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Ink(
              width: 64,
              height: 32,
              child: const Icon(Icons.home),
              decoration: (_selectedIndex == 0)
                  ? BoxDecoration(
                      color: const Color.fromRGBO(113, 247, 236, 1),
                      borderRadius: BorderRadius.circular(20))
                  : null,
            ),
            label: "Home",
          ),
          BottomNavigationBarItem(
            icon: Ink(
              width: 64,
              height: 32,
              child: const Icon(Icons.fitness_center),
              decoration: (_selectedIndex == 1)
                  ? BoxDecoration(
                      color: const Color.fromRGBO(113, 247, 236, 1),
                      borderRadius: BorderRadius.circular(20))
                  : null,
            ),
            label: "Routines",
          ),
          BottomNavigationBarItem(
            icon: Ink(
              width: 64,
              height: 32,
              child: const Icon(Icons.show_chart),
              decoration: (_selectedIndex == 2)
                  ? BoxDecoration(
                      color: const Color.fromRGBO(113, 247, 236, 1),
                      borderRadius: BorderRadius.circular(20))
                  : null,
            ),
            label: "Statistics",
          ),
          BottomNavigationBarItem(
            icon: Ink(
              width: 64,
              height: 32,
              child: const Icon(Icons.perm_identity),
              decoration: (_selectedIndex == 3)
                  ? BoxDecoration(
                      color: const Color.fromRGBO(113, 247, 236, 1),
                      borderRadius: BorderRadius.circular(20))
                  : null,
            ),
            label: "Profile",
          ),
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
