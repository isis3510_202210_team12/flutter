import 'ExerciseDetail.dart';
import 'package:firebase_database/firebase_database.dart';

class DaoArchitecture {
  
  final DatabaseReference _DaoRef = FirebaseDatabase.instance.reference()
      .child('mensajes');

  void saveM(DaoArchitecture mensaje) {
    _DaoRef.push().set(mensaje.hashCode);
  }

  Query getM()=> _DaoRef;
}