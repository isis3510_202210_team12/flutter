class HealthFormArguments {
  final bool justOne;
  final int selectedWidget;

  HealthFormArguments({required this.justOne, required this.selectedWidget});
}
