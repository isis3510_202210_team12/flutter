import '../interfaces/is_storagable.dart';

class RegisterData implements IsStorageable {
  final String email;
  final String name;
  RegisterData({required this.email, required this.name});

  factory RegisterData.fromJson(Map<String, dynamic> json) {
    return RegisterData(
      email: json['email'] as String,
      name: json['name'] as String,
    );
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      'email': email,
      'name': name,
    };
  }

  fromMap(Map<String, dynamic> map) {
    return RegisterData(
      email: map['email'] as String,
      name: map['name'] as String,
    );
  }
}
