class TimeSeriesWeight {
  final DateTime time;
  final int weight;

  TimeSeriesWeight({required this.time, required this.weight});
}
