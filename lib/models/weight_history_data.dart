import 'package:cloud_firestore/cloud_firestore.dart';

class row_data {
  Timestamp date;
  int weight;
  row_data({required this.date, required this.weight});
}

class WeightHistoryData {
  List<row_data> data;
  WeightHistoryData({required this.data});

  WeightHistoryData.fromFirestore(
    DocumentSnapshot<Map<String, dynamic>> snapshot,
    SnapshotOptions? options,
  ) : data = (snapshot.data()?['data'] as List<dynamic>).map((e) {
          return row_data(
            date: e['date'] as Timestamp,
            weight: e['weight'] as int,
          );
        }).toList();

  Map<String, dynamic> toFirestore() {
    return {
      'data': data.map((e) {
        return {
          'date': e.date,
          'weight': e.weight,
        };
      }).toList(),
    };
  }
}
