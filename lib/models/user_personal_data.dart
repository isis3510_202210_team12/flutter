import 'package:cloud_firestore/cloud_firestore.dart';

class UserPersonalData {
  int age;
  double weight;
  double height;
  String gender;

  UserPersonalData({
    required this.age,
    required this.weight,
    required this.height,
    required this.gender,
  });

  UserPersonalData.fromJson(Map<String, dynamic> json)
      : age = json['age'] as int,
        gender = json['gender'] as String,
        weight = json['weight'] as double,
        height = json['height'] as double;

  UserPersonalData.fromFirestore(
    DocumentSnapshot<Map<String, dynamic>> snapshot,
    SnapshotOptions? options,
  )   : age = snapshot.data()?['age'],
        gender = snapshot.data()?['gender'],
        weight = snapshot.data()?['weight'],
        height = snapshot.data()?['height'];

  Map<String, dynamic> toFirestore() {
    return {
      'age': age,
      'gender': gender,
      'weight': weight,
      'height': height,
    };
  }
}
