import 'package:hive/hive.dart';
part 'exercises_data.g.dart';

@HiveType(typeId: 0)
class ExercisesData extends HiveObject{ 

  @HiveField(0)
  late String name; 

  @HiveField(1)
  late String type; 

}