// ignore_for_file: non_constant_identifier_names

import 'package:cloud_firestore/cloud_firestore.dart';
import './user_personal_data.dart';

class UserData {
  String email;
  String last_routine;
  String last_workout;
  String name;
  double bpm;
  int age;
  String gender;
  String avatar_url;

  UserPersonalData? user_personal_data;

  UserData(
      {required this.email,
      required this.last_routine,
      required this.last_workout,
      required this.name,
      required this.age,
      required this.avatar_url,
      this.user_personal_data,
      required this.gender,
      required this.bpm});

  UserData.fromFirestore(
    DocumentSnapshot<Map<String, dynamic>> snapshot,
    SnapshotOptions? options,
  )   : email = snapshot.data()?['email'],
        user_personal_data =
            UserPersonalData.fromJson(snapshot.data()?['user_personal_data']),
        last_routine = snapshot.data()?['last_Routine'],
        last_workout = snapshot.data()?['last_workout'],
        avatar_url = snapshot.data()?['avatar_url'],
        name = snapshot.data()?['name'],
        age = snapshot.data()?['age'],
        gender = snapshot.data()?['gender'],
        bpm = snapshot.data()?['bpm'];

  Map<String, dynamic> toFirestore() {
    return {
      'email': email,
      'last_Routine': last_routine,
      'last_workout': last_workout,
      'name': name,
      'avatar_url': avatar_url,
      'user_personal_data': user_personal_data?.toFirestore(),
      'bpm': bpm,
    };
  }
}
