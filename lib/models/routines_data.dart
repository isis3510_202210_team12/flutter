import 'package:hive/hive.dart';
import '../models/exercises_data.dart';
part 'routines_data.g.dart';

@HiveType(typeId: 1)
class RoutinesData extends HiveObject{ 

  @HiveField(0)
  late String name; 

  @HiveField(1)
  late List<ExercisesData> exercises; 
}