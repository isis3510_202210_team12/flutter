abstract class ICalculateHealthStrategy {
  String calculateHealth(int age, double bpm);
}
