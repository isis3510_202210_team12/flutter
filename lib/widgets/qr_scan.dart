import 'dart:io';
import '../services/connection_service.dart';
import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:firebase_analytics/firebase_analytics.dart';

class QRScanPage extends StatefulWidget {
  static const routeName = '/qrScan';
  @override
  State<StatefulWidget> createState() => _QRScanPageState();
}

class _QRScanPageState extends State<QRScanPage> {

  var _isOnline = true;
  final ConnectionService _connectionService = ConnectionService();
  final qrKey = GlobalKey(debugLabel: 'QR');
  late QRViewController controller;
  FirebaseAnalytics analytics = FirebaseAnalytics.instance;
  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _connectionService.initialize();
    _connectionService.myStream.listen((event) {
      if (mounted) {
        setState(() {
          _isOnline = event;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    _connectionService.myStream.listen((event) {
      if (mounted) {
        setState(() {
          _isOnline = event;
        });
      }
    });
    return SafeArea(
      child: Scaffold(
        body: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            buildQrView(context),
            Positioned(bottom: 10, child: buildResult()),
            Positioned(
              top: 10,
              child: buildControlButtons(),
            )
          ],
        ),
      ),
    );
  }

  Widget buildQrView(BuildContext context) => QRView(
        key: qrKey,
        onQRViewCreated:onQRViewCreated,
        overlay: QrScannerOverlayShape(
          cutOutSize: MediaQuery.of(context).size.width * 0.8,
          borderWidth: 10,
          borderLength: 20,
          borderRadius: 10,
          borderColor: Colors.teal,
        ),
      );

  Widget buildControlButtons() => Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: Colors.white,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          IconButton(
              onPressed: () async {
                await controller.flipCamera();
                setState(() {});
              },
              icon: Icon(Icons.switch_camera)),
        ],
      ));

  Widget buildResult() => Container(
      padding: const EdgeInsets.all(12),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: Colors.white,
      ),
      child: const Text(
        'Scan a code!',
        maxLines: 3,
      ));

  void onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    _connectionService.myStream.listen((event) {
      if (mounted) {
        setState(() {
          _isOnline = event;
        });
      }
    });
    if (!_isOnline) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          backgroundColor: Colors.red,
          content: const Text("No internet connection"),
          action: SnackBarAction(
            textColor: Colors.white,
            label: 'Dismiss',
            onPressed: () {
              ScaffoldMessenger.of(context).hideCurrentSnackBar();
            },
          ),
          duration: const Duration(days: 365),
        ),
      );
    }
    else{
      controller.scannedDataStream.listen((scanData) async {
        controller.pauseCamera();
        analytics.logEvent(name: 'qr_scan', parameters: {
          'scan_data': scanData.code,
        });
        await launch(scanData.code);
        controller.resumeCamera();
    });
    }
  }
}
