import 'package:flutter/material.dart';
import '../screens/home_screen.dart';
import '../models/health_form_arguments.dart';
import '../models/user_data.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:permission_handler/permission_handler.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class TakePhoto extends StatefulWidget {
  const TakePhoto({Key? key}) : super(key: key);

  @override
  State<TakePhoto> createState() => _TakePhotoState();
}

class _TakePhotoState extends State<TakePhoto> {
  XFile? _imageFile;
  final _formKey = GlobalKey<FormState>();
  User? user = FirebaseAuth.instance.currentUser;
  FirebaseFirestore firestore = FirebaseFirestore.instance;

  void uploadToFirebase(bool justOne) async {
    String downloadUrl = "";

    final userRef = firestore
        .collection('users')
        .doc(user!.uid)
        .withConverter<UserData>(
            fromFirestore: UserData.fromFirestore,
            toFirestore: (UserData userData, _) => userData.toFirestore());

    if (_imageFile != null) {
      var file = File(_imageFile!.path);
      var snapshot = await FirebaseStorage.instance
          .ref()
          .child('users/${user!.uid}/${_imageFile?.name}')
          .putFile(file);
      downloadUrl = await snapshot.ref.getDownloadURL();

      userRef.update({
        'avatar_url': downloadUrl,
      });
    }

    if (justOne) {
      Navigator.of(context).pop();
    } else {
      Navigator.of(context).pushNamed(HomeScreen.routeName);
    }
  }

  uploadImage() async {
    final _imagePicker = ImagePicker();
    XFile? image;
    //Check Permissions
    await Permission.photos.request();
    var permissionStatus = await Permission.photos.status;
    if (permissionStatus.isGranted) {
      image = await _imagePicker.pickImage(source: ImageSource.camera);
      setState(() {
        _imageFile = image;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final args =
        ModalRoute.of(context)!.settings.arguments as HealthFormArguments;
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        (_imageFile != null)
            ? GestureDetector(
                child: CircleAvatar(
                  radius: 100,
                  backgroundImage: FileImage(File(_imageFile!.path)),
                ),
                onTap: () {
                  uploadImage();
                })
            : GestureDetector(
                child: const CircleAvatar(
                  radius: 100,
                  child: Icon(
                    Icons.camera_alt,
                    size: 100,
                  ),
                ),
                onTap: () {
                  uploadImage();
                }),
        const SizedBox(height: 20),
        const Text(
          "We want to know you",
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 25,
            fontWeight: FontWeight.w700,
          ),
        ),
        const SizedBox(height: 20),
        const Text(
          "Say cheers!",
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 25,
            fontWeight: FontWeight.w700,
          ),
        ),
        if (_imageFile != null)
          ElevatedButton(
            onPressed: () {
              uploadToFirebase(args.justOne);
            },
            child: const Text('Save'),
          ),
      ],
    );
  }
}
