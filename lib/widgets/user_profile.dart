import 'package:flutter/material.dart';
import '../screens/health_form_screen.dart';
import '../models/health_form_arguments.dart';
import '../models/user_data.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../services/user_data_service.dart';
import 'package:cached_network_image/cached_network_image.dart';

extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${this.substring(1).toLowerCase()}";
  }
}

class UserProfile extends StatefulWidget {
  UserProfile({Key? key}) : super(key: key);

  @override
  State<UserProfile> createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  FirebaseFirestore db = FirebaseFirestore.instance;

  User? user = FirebaseAuth.instance.currentUser;

  UserDataService userDataService = UserDataService();

  UserData? userData;

  void goToBPM(BuildContext context) {
    Navigator.pushNamed(context, HealthFormScreen.routeName,
            arguments: HealthFormArguments(
                justOne: true,
                selectedWidget: HealthFormScreen.widgetsIndex['checkBPM'] ?? 0))
        .then((value) {
      if (value == true) {
        setState(() {
          userData = userDataService.userData;
        });
      }
    });
  }

  @override
  void initState() {
    if (userDataService.userData != null) {
      userData = userDataService.userData;
    } else {
      db.collection('users').doc(user!.uid).get().then((value) {
        setState(() {
          userData = UserData.fromFirestore(value, null);
        });
      });
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return (userData != null)
        ? Center(
            child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Expanded(
                flex: 2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CircleAvatar(
                      radius: 100,
                      backgroundImage: CachedNetworkImageProvider(userData
                              ?.avatar_url ??
                          'https://thumbs.dreamstime.com/b/omita-al-avatar-placeholder-de-la-foto-icono-del-perfil-124557887.jpg'),
                    ),
                    const SizedBox(height: 10),
                    Text(
                      userData!.name.capitalize(),
                      style: const TextStyle(
                          fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    userData!.bpm != null
                        ? Text("Your current BPM is ${userData!.bpm} ")
                        : const Text(" Check your BPM"),
                    ElevatedButton(
                        onPressed: () {
                          goToBPM(context);
                        },
                        child: Text(userData?.bpm != null
                            ? "Check BPM again!"
                            : "Check your BPM"))
                  ],
                ),
              ),
              Expanded(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text(
                          "Your current weight is ${userData!.user_personal_data!.weight} kg",
                          style: const TextStyle(fontSize: 20)),
                      Text(
                          "Your height is ${userData!.user_personal_data!.height} cm",
                          style: const TextStyle(fontSize: 20)),
                      Text(
                          "You are ${userData!.user_personal_data!.age} years old",
                          style: const TextStyle(fontSize: 20)),
                      ElevatedButton(
                        onPressed: () {
                          Navigator.pushNamed(
                                  context, HealthFormScreen.routeName,
                                  arguments: HealthFormArguments(
                                      justOne: true,
                                      selectedWidget: HealthFormScreen
                                              .widgetsIndex['aboutYou'] ??
                                          0))
                              .then((value) {
                            if (value == true) {
                              setState(() {
                                userData = userDataService.userData;
                              });
                            }
                          });
                        },
                        child: const Text("Edit"),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ))
        : const Center(child: CircularProgressIndicator());
  }
}
