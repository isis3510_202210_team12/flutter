import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import '../models/time_series_weight.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import '../models/weight_history_data.dart';

class ChartWeight extends StatefulWidget {
  const ChartWeight({Key? key}) : super(key: key);

  @override
  State<ChartWeight> createState() => _ChartWeightState();
}

class _ChartWeightState extends State<ChartWeight> {
  FirebaseFirestore db = FirebaseFirestore.instance;
  User? user = FirebaseAuth.instance.currentUser;
  bool animate = true;

  Future<List<charts.Series<TimeSeriesWeight, DateTime>>> generateData() async {
    print('generateData');

    var historicalData =
        await db.collection('weight_history').doc(user!.uid).get();
    print("Voy a poner el primer resultado");
    print('historicalData ${historicalData.data()!['data'][0]}');
    final List<TimeSeriesWeight>? data;

    if (historicalData.data() != null) {
      data = historicalData.data()!['data'].map<TimeSeriesWeight>((e) {
        return TimeSeriesWeight(
          time: DateTime.fromMicrosecondsSinceEpoch(
              (e['date'] as Timestamp).microsecondsSinceEpoch),
          weight: e['weight'] as int,
        );
      }).toList();
    } else {
      data = [];
    }

    print('data');
    print('data: $data');

    return [
      charts.Series<TimeSeriesWeight, DateTime>(
        id: 'Weight',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (TimeSeriesWeight weight, _) => weight.time,
        measureFn: (TimeSeriesWeight weight, _) => weight.weight,
        data: data!,
      )
    ];
  }

  @override
  void initState() {
    generateData();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<charts.Series> seriesList;
    return FutureBuilder(
      future: generateData(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          if (snapshot.data != null) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  'Weight History',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(20),
                  height: 300,
                  child: charts.TimeSeriesChart(
                    snapshot.data as List<charts.Series<dynamic, DateTime>>,
                    animate: animate,
                    dateTimeFactory: const charts.LocalDateTimeFactory(),
                  ),
                ),
              ],
            );
          } else {
            return Container(
              child: Text('No data'),
            );
          }
        } else {
          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
      },
    );
  }
}
