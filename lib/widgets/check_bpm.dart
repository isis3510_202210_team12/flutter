import 'package:flutter/material.dart';
import 'package:heart_bpm/heart_bpm.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../models/health_form_arguments.dart';
import '../models/user_data.dart';
import '../utils/healthStrategy/man_health_strategy.dart';
import '../utils/healthStrategy/woman_health_strategy.dart';
import '../utils/healthStrategy/context_calculate_health.dart';

class CheckBPM extends StatefulWidget {
  VoidCallback onNext;

  CheckBPM({Key? key, required this.onNext}) : super(key: key);

  @override
  State<CheckBPM> createState() => _CheckBPMState();
}

class _CheckBPMState extends State<CheckBPM> {
  List<SensorValue> data = [];
  List<SensorValue> bpmValues = [];
  List<double> bpm = [];

  FirebaseFirestore db = FirebaseFirestore.instance;

  bool measurementIsDone = false;

  ContextCalculateHealth contextCalculateHealth = ContextCalculateHealth();

  double bpmAvg = 0;

  User? user = FirebaseAuth.instance.currentUser;

  DocumentSnapshot<UserData>? userData;

  double currentBPM = 0;

  bool isBPMEnabled = false;

  void handleMeasurement() async {
    stopBPM();

    bpmAvg = bpm.reduce((value, element) {
          return value + element;
        }) /
        bpm.length;

    await db.collection('users').doc(user!.uid).update({
      'bpm': bpmAvg.roundToDouble(),
    });

    userData = await db
        .collection('users')
        .withConverter(
            fromFirestore: UserData.fromFirestore,
            toFirestore: (UserData userData, _) => userData.toFirestore())
        .doc(user!.uid)
        .get();

    if (mounted) {
      setState(() {
        measurementIsDone = true;
      });
    }
  }

  Widget getHealth() {
    final age = userData!.data()?.age ?? 18;
    final gender = userData!.data()?.gender ?? 'woman';

    final concreteStrategy =
        gender == 'woman' ? WomanHealthStrategy() : ManHealthStrategy();

    contextCalculateHealth.setStrategy(concreteStrategy);

    return Text(
        "You  bpm health is: ${contextCalculateHealth.calculateHealth(age, bpmAvg)} ");
  }

  void stopBPM() {
    if (mounted) {
      setState(() {
        isBPMEnabled = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final args =
        ModalRoute.of(context)!.settings.arguments as HealthFormArguments;

    return Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
          measurementIsDone
              ? Column(
                  children: [
                    Text(" Your average BPM is: ${bpmAvg.toStringAsFixed(2)}"),
                    getHealth(),
                  ],
                )
              : isBPMEnabled
                  ? HeartBPMDialog(
                      context: context,
                      onRawData: (value) {
                        setState(() {
                          if (data.length >= 100) data.removeAt(0);
                          data.add(value);
                        });
                      },
                      onBPM: (value) => setState(() {
                            if (bpmValues.length >= 100) bpmValues.removeAt(0);
                            bpmValues.add(SensorValue(
                                value: value.toDouble(), time: DateTime.now()));
                            bpm.add(value.toDouble());
                          }))
                  : SizedBox(
                      width: 0.8 * MediaQuery.of(context).size.width,
                      child: const Text(
                          'To measure your BPM please press start and put your finger on the camera.',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 20))),
          isBPMEnabled
              ? const SizedBox()
              : measurementIsDone
                  ? SizedBox()
                  : ElevatedButton(
                      onPressed: () {
                        if (mounted) {
                          setState(() {
                            isBPMEnabled = !isBPMEnabled;
                            Future.delayed(
                                const Duration(seconds: 10), handleMeasurement);
                          });
                        }
                      },
                      child: Text('Start'),
                    ),
          if (measurementIsDone && args.justOne)
            ElevatedButton(
              onPressed: () {
                Navigator.pop(context, true);
              },
              child: const Text('Save'),
            )
          else if (measurementIsDone && !args.justOne)
            ElevatedButton(
              onPressed: () {
                widget.onNext();
              },
              child: const Text('Next'),
            )
        ]));
  }
}
