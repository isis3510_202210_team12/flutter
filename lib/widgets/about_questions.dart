import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../services/user_data_service.dart';
import '../models/health_form_arguments.dart';
import '../models/user_personal_data.dart';

class AboutQuestions extends StatefulWidget {
  VoidCallback onNext;
  AboutQuestions({Key? key, required this.onNext}) : super(key: key);

  @override
  State<AboutQuestions> createState() => _AboutQuestionsState();
}

class _AboutQuestionsState extends State<AboutQuestions> {
  final _formKey = GlobalKey<FormState>();
  UserDataService userDataService = UserDataService();
  final _ageController = TextEditingController();
  final _weightController = TextEditingController();
  final _heightController = TextEditingController();
  final _genderController = TextEditingController(text: '');

  String? _checkAge(String? age) {
    if (age!.isEmpty) {
      return 'Please enter your age';
    } else if (int.tryParse(age) == null) {
      return 'Please enter a valid age';
    } else if (int.tryParse(age)! < 0) {
      return 'Please enter a valid age';
    } else if (int.tryParse(age)! > 120) {
      return 'Please enter a valid age';
    }
    return null;
  }

  String? _checkWeight(String? weight) {
    if (weight!.isEmpty) {
      return 'Please enter your weight';
    } else if (double.tryParse(weight) == null) {
      return 'Please enter a valid weight';
    } else if (double.tryParse(weight)! < 0) {
      return 'Please enter a valid weight';
    }

    return null;
  }

  User? user = FirebaseAuth.instance.currentUser;
  FirebaseFirestore db = FirebaseFirestore.instance;
  String? _checkHeight(String? height) {
    if (height!.isEmpty) {
      return 'Please enter your height';
    } else if (double.tryParse(height) == null) {
      return 'Please enter a valid height';
    } else if (double.tryParse(height)! < 0) {
      return 'Please enter a valid height';
    } else if (double.tryParse(height)! > 220) {
      return 'Please enter a valid height';
    }
    return null;
  }

  String? _checkGender(String? gender) {
    if (gender!.isEmpty) {
      return 'Please enter your gender';
    }
    return null;
  }

  final optionsGender = [
    const DropdownMenuItem(
      child: Text('Male'),
      value: 'Male',
    ),
    const DropdownMenuItem(
      child: Text('Female'),
      value: 'Female',
    )
  ];

  Future<void> _submitData() async {
    final userPersonalData = UserPersonalData(
        age: int.parse(_ageController.text),
        weight: double.parse(_weightController.text),
        height: double.parse(_heightController.text),
        gender: _genderController.text);

    await db
        .collection('users')
        .doc(user!.uid)
        .update({'user_personal_data': userPersonalData.toFirestore()});

    await db.collection('weight_history').doc(user!.uid).update({
      'data': FieldValue.arrayUnion([
        {'weight': int.parse(_weightController.text), 'date': DateTime.now()}
      ])
    });

    await userDataService.retrieveUserData();
  }

  void _handleSubmit(bool justOne, BuildContext ctx) async {
    await _submitData();
    if (justOne) {
      Navigator.pop(context, true);
    } else {
      print("Hago esto");
      widget.onNext();
    }
  }

  @override
  void initState() {
    if (userDataService.userData != null) {
      _ageController.text =
          userDataService.userData!.user_personal_data!.age.toString();
      _weightController.text =
          userDataService.userData!.user_personal_data!.weight.toString();
      _heightController.text =
          userDataService.userData!.user_personal_data!.height.toString();
      _genderController.text =
          userDataService.userData!.user_personal_data!.gender.toString();
    }
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final args =
        ModalRoute.of(context)!.settings.arguments as HealthFormArguments;

    return Form(
      key: _formKey,
      child: SizedBox(
        width: 250,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Text(
              "About yourself",
              style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(63, 73, 71, 1),
                  letterSpacing: 0.1),
            ),
            TextFormField(
              controller: _ageController,
              validator: _checkAge,
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
              ],
              decoration: InputDecoration(
                  errorMaxLines: 2,
                  border: const OutlineInputBorder(),
                  labelText: 'Age',
                  suffixIcon: IconButton(
                    icon: const Icon(
                      Icons.cancel,
                    ),
                    onPressed: () {
                      _ageController.clear();
                    },
                  )),
            ),
            TextFormField(
              controller: _heightController,
              validator: _checkHeight,
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.allow(RegExp(r'[0-9]|\.')),
              ],
              decoration: InputDecoration(
                  errorMaxLines: 2,
                  border: const OutlineInputBorder(),
                  labelText: 'Height',
                  suffix: const Text('cm'),
                  suffixIcon: IconButton(
                    icon: const Icon(
                      Icons.cancel,
                    ),
                    onPressed: () {
                      _ageController.clear();
                    },
                  )),
            ),
            TextFormField(
              controller: _weightController,
              validator: _checkWeight,
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.allow(RegExp(r'[0-9]|\.')),
              ],
              decoration: InputDecoration(
                  errorMaxLines: 2,
                  border: const OutlineInputBorder(),
                  labelText: 'Weight',
                  suffix: const Text('kg'),
                  suffixIcon: IconButton(
                    icon: const Icon(
                      Icons.cancel,
                    ),
                    onPressed: () {
                      _ageController.clear();
                    },
                  )),
            ),
            if (!args.justOne)
              DropdownButtonFormField(
                items: optionsGender,
                validator: _checkGender,
                hint: const Text('Gender'),
                onChanged: (value) {
                  setState(() {
                    _genderController.text = value as String;
                  });
                },
              ),
            ElevatedButton(
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  _handleSubmit(args.justOne, context);
                }
              },
              child:
                  (args.justOne) ? const Text('Save') : const Text('Continue'),
              style: ElevatedButton.styleFrom(
                shape: const StadiumBorder(),
                minimumSize: const Size(100, 40),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
