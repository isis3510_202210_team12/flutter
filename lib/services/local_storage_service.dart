import 'package:localstorage/localstorage.dart';
import '../interfaces/is_storagable.dart';
import '../models/register_data.dart';

class LocalStorageService {
  static final LocalStorageService _instance = LocalStorageService._internal();
  factory LocalStorageService() => _instance;
  LocalStorageService._internal();

  final LocalStorage storage = LocalStorage('gymtastic');

  Future<void> setItem(String key, IsStorageable item) async {
    return storage.setItem(key, item.toJson());
  }

  Future<RegisterData?> getRegisterData() async {
    final data = await storage.getItem('registerData');
    if (data == null) {
      return null;
    }
    return RegisterData.fromJson(data);
  }

  Future<void> removeItem(String key) async {
    return storage.deleteItem(key);
  }
}
