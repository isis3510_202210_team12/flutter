import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import '../models/user_data.dart';
import 'package:flutter/material.dart';

class UserDataService {
  static final UserDataService _instance = UserDataService._internal();
  final navigatorKey = new GlobalKey<NavigatorState>();
  UserData? _userData;
  String? _userId;
  factory UserDataService() {
    return _instance;
  }

  void initializeService() {
    FirebaseAuth.instance.authStateChanges().listen((User? user) {
      print("User: $user");
      if (user == null) {
        cleanUserData();

        navigatorKey.currentState!.pushReplacementNamed('/login');
      } else {
        _userId = user.uid;
        retrieveUserData();
      }
    });
  }

  cleanUserData() {
    _userData = null;
  }

  Future<void> retrieveUserData() async {
    _userData = await FirebaseFirestore.instance
        .collection('users')
        .withConverter<UserData>(
            fromFirestore: UserData.fromFirestore,
            toFirestore: (UserData userData, _) => userData.toFirestore())
        .doc(_userId)
        .get()
        .then((snapshot) => snapshot.data()!);
  }

  UserData? get userData => _userData;

  UserDataService._internal();
}
