import 'dart:io';
import 'dart:async';
import 'package:connectivity_plus/connectivity_plus.dart';

class ConnectionService {
  static final ConnectionService _instance = ConnectionService._internal();

  factory ConnectionService() {
    return _instance;
  }

  ConnectionService._internal();

  final _connectivity = Connectivity();
  final _controller = StreamController.broadcast();
  Stream get myStream => _controller.stream;

  void initialize() async {
    ConnectivityResult result = await _connectivity.checkConnectivity();
    _checkStatus(result);
    _connectivity.onConnectivityChanged.listen((result) {
      _checkStatus(result);
    });
  }

  void _checkStatus(ConnectivityResult result) async {
    bool isOnline = false;

    if (result == ConnectivityResult.none) {
      isOnline = false;
      if (!_controller.isClosed) {
        _controller.sink.add(isOnline);
      }
    } else {
      try {
        final response = await InternetAddress.lookup('google.com');

        isOnline = response.isNotEmpty && response[0].rawAddress.isNotEmpty;
      } on SocketException catch (_) {
        isOnline = false;
      }
      if (!_controller.isClosed) {
        _controller.sink.add(isOnline);
      }
    }
  }

  void disposeStream() => _controller.close();
}
