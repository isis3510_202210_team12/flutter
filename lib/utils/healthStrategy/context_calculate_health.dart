import '../../interfaces/calculate_health_strategy.dart';
import './woman_health_strategy.dart';

class ContextCalculateHealth {
  ICalculateHealthStrategy _calculateHealthStrategy = new WomanHealthStrategy();

  ContextCalculateHealth();

  void setStrategy(ICalculateHealthStrategy strategy) {
    _calculateHealthStrategy = strategy;
  }

  String calculateHealth(int age, double bpm) {
    return _calculateHealthStrategy.calculateHealth(age, bpm);
  }
}
