import '../../interfaces/calculate_health_strategy.dart';

class ManHealthStrategy implements ICalculateHealthStrategy {
  @override
  String calculateHealth(int age, double bpm) {
    if (age >= 18 && age <= 25) {
      if (bpm >= 49 && bpm <= 55) {
        return 'athlete';
      } else if (bpm >= 56 && bpm <= 61) {
        return 'excellent';
      } else if (bpm >= 62 && bpm <= 65) {
        return 'great';
      } else if (bpm >= 66 && bpm <= 69) {
        return 'good';
      } else if (bpm >= 70 && bpm <= 73) {
        return 'average';
      } else if (bpm >= 74 && bpm <= 81) {
        return 'below average';
      } else {
        return 'poor';
      }
    } else if (age >= 26 && age <= 36) {
      if (bpm >= 49 && bpm <= 54) {
        return 'athlete';
      } else if (bpm >= 55 && bpm <= 61) {
        return 'excellent';
      } else if (bpm >= 62 && bpm <= 65) {
        return 'great';
      } else if (bpm >= 66 && bpm <= 70) {
        return 'good';
      } else if (bpm >= 71 && bpm <= 74) {
        return 'average';
      } else if (bpm >= 75 && bpm <= 81) {
        return 'below average';
      } else {
        return 'poor';
      }
    } else if (age >= 36 && age <= 45) {
      if (bpm >= 50 && bpm <= 56) {
        return 'athlete';
      } else if (bpm >= 57 && bpm <= 62) {
        return 'excellent';
      } else if (bpm >= 63 && bpm <= 66) {
        return 'great';
      } else if (bpm >= 67 && bpm <= 70) {
        return 'good';
      } else if (bpm >= 71 && bpm <= 75) {
        return 'average';
      } else if (bpm >= 76 && bpm <= 82) {
        return 'below average';
      } else {
        return 'poor';
      }
    } else if (age >= 46 && age <= 55) {
      if (bpm >= 50 && bpm <= 57) {
        return 'athlete';
      } else if (bpm >= 58 && bpm <= 63) {
        return 'excellent';
      } else if (bpm >= 64 && bpm <= 67) {
        return 'great';
      } else if (bpm >= 68 && bpm <= 71) {
        return 'good';
      } else if (bpm >= 72 && bpm <= 76) {
        return 'average';
      } else if (bpm >= 77 && bpm <= 83) {
        return 'below average';
      } else {
        return 'poor';
      }
    } else if (age >= 56 && age <= 65) {
      if (bpm >= 51 && bpm <= 56) {
        return 'athlete';
      } else if (bpm >= 57 && bpm <= 61) {
        return 'excellent';
      } else if (bpm >= 62 && bpm <= 67) {
        return 'great';
      } else if (bpm >= 68 && bpm <= 71) {
        return 'good';
      } else if (bpm >= 72 && bpm <= 75) {
        return 'average';
      } else if (bpm >= 76 && bpm <= 81) {
        return 'below average';
      } else {
        return 'poor';
      }
    } else {
      if (bpm >= 50 && bpm <= 55) {
        return 'athlete';
      } else if (bpm >= 56 && bpm <= 61) {
        return 'excellent';
      } else if (bpm >= 62 && bpm <= 65) {
        return 'great';
      } else if (bpm >= 66 && bpm <= 69) {
        return 'good';
      } else if (bpm >= 70 && bpm <= 73) {
        return 'average';
      } else if (bpm >= 74 && bpm <= 79) {
        return 'below average';
      } else {
        return 'poor';
      }
    }
  }
}
