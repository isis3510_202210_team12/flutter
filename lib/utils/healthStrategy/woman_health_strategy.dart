import '../../interfaces/calculate_health_strategy.dart';

class WomanHealthStrategy implements ICalculateHealthStrategy {
  @override
  String calculateHealth(int age, double bpm) {
    if (age >= 18 && age <= 25) {
      if (bpm >= 54 && bpm <= 60) {
        return 'athlete';
      } else if (bpm >= 61 && bpm <= 65) {
        return 'excellent';
      } else if (bpm >= 66 && bpm <= 69) {
        return 'great';
      } else if (bpm >= 70 && bpm <= 73) {
        return 'good';
      } else if (bpm >= 74 && bpm <= 78) {
        return 'average';
      } else if (bpm >= 79 && bpm <= 84) {
        return 'below average';
      } else {
        return 'poor';
      }
    } else if (age >= 26 && age <= 36) {
      if (bpm >= 54 && bpm <= 59) {
        return 'athlete';
      } else if (bpm >= 60 && bpm <= 64) {
        return 'excellent';
      } else if (bpm >= 65 && bpm <= 68) {
        return 'great';
      } else if (bpm >= 69 && bpm <= 72) {
        return 'good';
      } else if (bpm >= 73 && bpm <= 76) {
        return 'average';
      } else if (bpm >= 77 && bpm <= 82) {
        return 'below average';
      } else {
        return 'poor';
      }
    } else if (age >= 36 && age <= 45) {
      if (bpm >= 54 && bpm <= 59) {
        return 'athlete';
      } else if (bpm >= 60 && bpm <= 64) {
        return 'excellent';
      } else if (bpm >= 65 && bpm <= 69) {
        return 'great';
      } else if (bpm >= 70 && bpm <= 73) {
        return 'good';
      } else if (bpm >= 74 && bpm <= 78) {
        return 'average';
      } else if (bpm >= 79 && bpm <= 84) {
        return 'below average';
      } else {
        return 'poor';
      }
    } else if (age >= 46 && age <= 55) {
      if (bpm >= 54 && bpm <= 60) {
        return 'athlete';
      } else if (bpm >= 61 && bpm <= 65) {
        return 'excellent';
      } else if (bpm >= 66 && bpm <= 69) {
        return 'great';
      } else if (bpm >= 70 && bpm <= 73) {
        return 'good';
      } else if (bpm >= 74 && bpm <= 77) {
        return 'average';
      } else if (bpm >= 78 && bpm <= 83) {
        return 'below average';
      } else {
        return 'poor';
      }
    } else if (age >= 56 && age <= 65) {
      if (bpm >= 54 && bpm <= 59) {
        return 'athlete';
      } else if (bpm >= 60 && bpm <= 64) {
        return 'excellent';
      } else if (bpm >= 65 && bpm <= 68) {
        return 'great';
      } else if (bpm >= 69 && bpm <= 73) {
        return 'good';
      } else if (bpm >= 74 && bpm <= 77) {
        return 'average';
      } else if (bpm >= 78 && bpm <= 83) {
        return 'below average';
      } else {
        return 'poor';
      }
    } else {
      if (bpm >= 54 && bpm <= 59) {
        return 'athlete';
      } else if (bpm >= 60 && bpm <= 64) {
        return 'excellent';
      } else if (bpm >= 65 && bpm <= 68) {
        return 'great';
      } else if (bpm >= 69 && bpm <= 72) {
        return 'good';
      } else if (bpm >= 73 && bpm <= 76) {
        return 'average';
      } else if (bpm >= 77 && bpm <= 84) {
        return 'below average';
      } else {
        return 'poor';
      }
    }
  }
}
