// ignore_for_file: prefer_const_constructors

import 'dart:async';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:gymtastic/screens/routines_screen.dart';
import 'models/routines_data.dart';
import 'screens/health_form_screen.dart';
import 'package:gymtastic/widgets/qr_scan.dart';
import 'screens/home_screen.dart';
import 'screens/landing_screen.dart';
import 'screens/login_screen.dart';
import 'screens/register_screen.dart';
import 'services/connection_service.dart';
import 'firebase_options.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import './models/exercises_data.dart';
import '../services/user_data_service.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Hive.initFlutter();
  Hive.registerAdapter(ExercisesDataAdapter());
  await Hive.openBox<ExercisesData>('exercises_local');
  Hive.registerAdapter(RoutinesDataAdapter());
  await Hive.openBox<RoutinesData>('routines_local');

  runZonedGuarded<Future<void>>(() async {
    WidgetsFlutterBinding.ensureInitialized();
    await Firebase.initializeApp(
      options: DefaultFirebaseOptions.currentPlatform,
    );
    UserDataService userDataService = UserDataService();
    userDataService.initializeService();
    final connectionService = ConnectionService();
    connectionService.initialize();
    FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
    runApp(MyApp());
  }, (error, stack) => FirebaseCrashlytics.instance.recordError(error, stack));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Gymtastic',
      theme: ThemeData(
          primarySwatch: Colors.teal,
          colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.teal)
              .copyWith(secondary: const Color.fromRGBO(98, 189, 181, 0)),
          canvasColor: Color.fromRGBO(255, 255, 255, 1),
          textTheme: ThemeData.light().textTheme.copyWith(
                bodyText1:
                    const TextStyle(color: Color.fromRGBO(20, 51, 51, 1)),
                bodyText2:
                    const TextStyle(color: Color.fromRGBO(20, 51, 51, 1)),
                headline6: const TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              )),
      home: LandingScreen(),
      routes: {
        QRScanPage.routeName: (context) => QRScanPage(),
        LandingScreen.routeName: (context) => LandingScreen(),
        HomeScreen.routeName: (context) => HomeScreen(),
        RegisterScreen.routeName: (context) => RegisterScreen(),
        LoginScreen.routeName: (context) => LoginScreen(),
        HealthFormScreen.routeName: (context) => HealthFormScreen(),
        RoutineScreen.routeName:(context) => RoutineScreen(),
      },
    );
  }
  // home: CategoriesScreen(),
  // routes: {
  //   CategoryMealsScreen.routeName: (ctx) => CategoryMealsScreen(),
  // });
}
